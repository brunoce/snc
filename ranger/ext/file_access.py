import os
from ranger.api.hierarchy_access import HierarchyAccess

class FileAccess(HierarchyAccess):
    tag = 'file'
    sep = os.path.sep
    def exists(self, value):
        return os.path.exists(value)

    def lexists(self, value):
        return os.path.lexists(value)

    def stat(self, value):
        return os.stat(value)

    def makedirs(self, value):
        return os.makedirs(value)

    def mkdir(self, value):
        return os.mkdir(value)

    def access(self, value):
        return os.access(value, os.F_OK)

    def rename(self, value):
        return os.rename(value)

    def remove(self, value):
        return os.remove(value)

    def chmod(self, value, mode):
        return os.chmod(value, mode)

    def readlink(self, value):
        return os.readlink(value)

    def symlink(self, new_value, value):
        return os.symlink(new_value, value)

    def unlink(self, value):
        return os.unlink(value)

    def getmtime(self, value):
        return os.path.getmtime(value)

    def isfile(self, value):
        return os.path.isfile(value)

    def isdir(self, value):
        return os.path.isdir(value)

    def islink(self, value):
        return os.path.islink(value)

    def isabs(self, value):
        return os.path.isabs(value)

    def ismount(self, value):
        return os.path.ismount(value)

    def normpath(self, value):
        return os.path.normpath(value)

    def realpath(self, value):
        return os.path.realpath(value)

    def relpath(self, value):
        return os.path.relpath(value)

    def abspath(self, value):
        return os.path.abspath(value)

    def basename(self, value):
        return os.path.basename(value)

    def dirname(self, value):
        return os.path.dirname(value)

    def expanduser(self, value):
        return os.path.expanduser(value)

    def split(self, value):
        return os.path.split(value)

    def join(self, value, *paths):
        return os.path.join(value, *paths)

    def listdir(self, top):
        return os.listdir(top)

    def walk(self, top):
        return os.walk(top)
