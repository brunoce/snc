class HierarchyAccess(object):
    """Base class for all hierarchical access classes"""

    # I'm not sure if the .py file name needs "base" as prefix
    # as it's part of api
    @property
    def tag(self):
        return self.__class__

    def get_name(self):
        return self.tag

    @property
    def sep(self):
        raise NotImplementedError('This function must be overriden.')

    def exists(self, value):
        raise NotImplementedError('This function must be overriden.')

    def lexists(self, value):
        raise NotImplementedError('This function must be overriden.')

    def stat(self, value):
        raise NotImplementedError('This function must be overriden.')

    def makedirs(self, value):
        raise NotImplementedError('This function must be overriden.')

    def mkdir(self, value):
        raise NotImplementedError('This function must be overriden.')

    def access(self, value):
        raise NotImplementedError('This function must be overriden.')

    def rename(self, value):
        raise NotImplementedError('This function must be overriden.')

    def chmod(self, value, mode):
        raise NotImplementedError('This function must be overriden.')

    def readlink(self, value):
        raise NotImplementedError('This function must be overriden.')

    def symlink(self, new_value, value):
        raise NotImplementedError('This function must be overriden.')

    def unlink(self, value):
        raise NotImplementedError('This function must be overriden.')

    def remove(self, value):
        raise NotImplementedError('This function must be overriden.')

    def getmtime(self, value):
        raise NotImplementedError('This function must be overriden.')

    def isfile(self, value):
        raise NotImplementedError('This function must be overriden.')

    def isdir(self, value):
        raise NotImplementedError('This function must be overriden.')

    def islink(self, value):
        raise NotImplementedError('This function must be overriden.')

    def isabs(self, value):
        raise NotImplementedError('This function must be overriden.')

    def ismount(self, value):
        raise NotImplementedError('This function must be overriden.')

    def normpath(self, value):
        raise NotImplementedError('This function must be overriden.')

    def realpath(self, value):
        raise NotImplementedError('This function must be overriden.')

    def relpath(self, value):
        raise NotImplementedError('This function must be overriden.')

    def abspath(self, value):
        raise NotImplementedError('This function must be overriden.')

    def basename(self, value):
        raise NotImplementedError('This function must be overriden.')

    def dirname(self, value):
        raise NotImplementedError('This function must be overriden.')

    def expanduser(self, value):
        raise NotImplementedError('This function must be overriden.')

    def split(self, value):
        raise NotImplementedError('This function must be overriden.')

    def join(self, value, *paths):
        raise NotImplementedError('This function must be overriden.')

    def listdir(self, top):
        raise NotImplementedError('This function must be overriden.')

    def walk(self, top):
        raise NotImplementedError('This function must be overriden.')
